var request = require('request-json');
var config = require('../config/config.js');
const mlabConnection = config.connection;


// GET Accounts
function getAccounts(req, res) {
  console.log('GET /accounts');
  if (req.query.user_id) {
    req.query.user_id = parseInt(req.query.user_id);
  }
  query = `q=${JSON.stringify(req.query)}&f={"_id": 0}`;
  dbClient = mlabConnection('accounts', query);
  dbClient.get('', function (errM, resM, bodyM) {
    let response;
    if (errM) {
      response = { "msg" : "Error consultado cuentas." };
      code = 500;
    } else {
      response = bodyM;
      code = 200;
    };

    res.status(code).send(response);
  });
}

// GET Account id
function getAccountByID(req, res) {
  console.log('GET /accounts/:id');
  query = `q={"id": ${req.params.id}}&f={"_id": 0}`;
  dbClient = mlabConnection('accounts', query);
  dbClient.get('', function (errM, resM, bodyM) {
    let response;
    if (errM) {
      response = { "msg" : "Error obteniendo cuenta." };
      code = 500;
    } else if (bodyM.length > 0) {
      response = bodyM;
      code = 200;
    } else {
      response = {msg : "Cuenta no válida."};
      code = 404;
    };

    res.status(code).send(response);
  });
}

// POST Account
function postAccount(req, res) {
  console.log('POST /accounts');

  let newAccount = {
    "user_id": req.body.user_id,
    "balance": "$0",
    "iban": getIBAN()
  }

  query = `f={"_id": 0, "id": 1}&s={"id": -1}&l=1`;
  dbClient = mlabConnection('accounts', query);
  dbClient.get('', function(errG, resG, bodyG) {
    let nextID = bodyG[0].id + 1;
    newAccount.id = nextID;

    mlabConnection('accounts', query).post('', newAccount, function(errP, resP, bodyP) {
      let response;
      if (errP) {
        response = { "msg" : "Error agregando cuenta." };
        code = 500;
      } else {
        response = newUser;
        code = 200;
      };

      res.status(code).send(response);
    });
  });

}

// PUT Account
function putAccount(req, res) {
  console.log('PUT /accounts');
  res.send('ok');
}

// DELETE Account
function deleteAccount(req, res) {
  console.log('DELETE /accounts');
  res.send('ok');
}

// GET Transactions of Account
function getTransactionsByAccount (req, res) {
  console.log('GET /accounts/:id/transaccions');

  acc = parseInt(req.params.id);
  q = {$or: [ {"src": acc}, {"dst": acc} ] };
  query = `q=${JSON.stringify(q)}&f={"_id": 0}`;
  dbClient = mlabConnection('movements', query);
  dbClient.get('', function (errM, resM, bodyM) {
    let response;
    if (errM) {
      response = { "msg" : "Error consultado movimientos." };
      code = 500;
    } else {
      response = bodyM;
      code = 200;
    };

    res.status(code).send(response);
  });

}

// Generate random iban.
function getIBAN() {

  let iban = '';
  let chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

  for (var i = 0; i < 7; i++){
    for (var j = 0; j < 4; j++) {
        iban += chars.charAt(Math.floor(Math.random() * chars.length));
    }
    iban += ' ';
  }

  return iban
}

// Exports
module.exports.getAccounts = getAccounts;
module.exports.getAccount = getAccountByID;
module.exports.postAccount = postAccount;
module.exports.putAccount = putAccount;
module.exports.deleteAccount = deleteAccount;
module.exports.getTransactionsAccount = getTransactionsByAccount;
module.exports.getIBAN = getIBAN;
