var request = require('request-json');
var config = require('../config/config.js');
var jwtconfig = require('../config/jwtconfig.js');
var accounts = require('../app/accounts.js');
const mlabkey = config.mlabkey;
const mlabdbURI = config.mlabdbURI;
const mlabConnection = config.connection;


/**** AUTH ****/

// LOGIN w/token
function loginToken(req, res) {
  console.log('POST /login');
  let email = req.body.email;
  let pass  = req.body.password;
  if (email && pass) {
    dbClient = request.createClient(mlabdbURI);
    query = `q={email:${JSON.stringify(email)}}`;
    dbClient.get(`users?${query}&${mlabkey}`, function (errGet, resGet, bodyGet) {
      let user = bodyGet[0]
      if (user) {
        if (pass == user.password) {
          let token = jwtconfig.sign({ userID: user.id });
          res.status(200).send({ "msg" : "Successfull login.", "data": {"token" : token, "user_id" : user.id } });
        } else {
          res.status(404).send({ "msg" : "Wrong credentials." });
        };
      } else {
        res.status(404).send({ "msg" : "Wrong credentials." });
      };
    });
  } else {
    res.status(404).send({ "msg" : "Wrong credentials." });
  };
};


//LOGOUT w/token
function logoutToken(req, res) {
  console.log('POST /logout (token)');
  token = req.headers.authorization.split(' ')[1];
  jti = jwtconfig.decode(token).jti;
  revoked = {'tokenid' : jti};
  dbClient = request.createClient(mlabdbURI);
  dbClient.post(`blacklist?${mlabkey}`, revoked, function(errPost, resPost, bodyPost) {
    if (!errPost) {
      res.status(200).send({ "msg" : "Successfull logout." });
    } else {
      res.status(500).send({ "msg" : "Error." });
    }
  });
}


//SIGIN
function signin(req, res) {
  console.log('POST /sigin');

  let newUser = {
    'first_name': req.body.first_name,
    'last_name': req.body.last_name,
    'email': req.body.email,
    'password': req.body.password
  };

  dbClient = request.createClient(mlabdbURI);
  query = `f={"_id": 0, "id": 1}&s={"id": -1}&l=1`;
  dbClient.get(`users?${query}&${mlabkey}`, function (errM, resM, bodyM) {
    let response;
    let nextID = bodyM[0].id + 1;
    newUser["id"] = nextID;

    dbClient.post(`users?&${mlabkey}`, newUser, function (errorP, resP, bodyP) {
      if (errorP) {
        res.status(500).send({ "msg" : "Error agregando usuario." });
      } else {

          let newAccount = {
            "user_id": newUser.id,
            "balance": "$0",
            "iban": accounts.getIBAN()
          }

          query = `f={"_id": 0, "id": 1}&s={"id": -1}&l=1`;
          dbClient = mlabConnection('accounts', query);
          dbClient.get('', function(errG, resG, bodyG) {
            let nextID = bodyG[0].id + 1;
            newAccount.id = nextID;

            console.log(newAccount);

            mlabConnection('accounts', query).post('', newAccount, function(errPA, resPA, bodyPA) {
              let response;
              if (errPA) {
                response = { "msg" : "Error creando cuenta." };
                code = 500;
              } else {
                let token = jwtconfig.sign({ userID: newUser.id });
                response = { "msg" : "Successfull login.", "data": {"token" : token, "user_id" : newUser.id } };
                code = 200
              };

              res.status(code).send(response);
            });
          });
      };
    });
  });

}


// Exports
module.exports.login = loginToken;
module.exports.logout = logoutToken;
module.exports.signin = signin;
