var request = require('request-json');
var config = require('../config/config.js');
const mlabConnection = config.connection;


// GET Accounts
function getAccounts(req, res) {
  console.log('GET /accounts');
  if (req.query.user_id) {
    req.query.user_id = parseInt(req.query.user_id);
  }
  query = `q=${JSON.stringify(req.query)}&f={"_id": 0}`;
  dbClient = mlabConnection('accounts', query);
  dbClient.get('', function (errM, resM, bodyM) {
    let response;
    if (errM) {
      response = { "msg" : "Error consultado cuentas." };
      code = 500;
    } else {
      response = bodyM;
      code = 200;
    };

    res.status(code).send(response);
  });
}


// POST Account
function postAccount(req, res) {
  console.log('POST /accounts');

  let newAccount = {
    "user_id": req.body.user_id,
    "balance": "$0",
    "iban": getIBAN()
  }

  query = `f={"_id": 0, "id": 1}&s={"id": -1}&l=1`;
  dbClient = mlabConnection('accounts', query);
  dbClient.get('', function(errG, resG, bodyG) {
    let nextID = bodyG[0].id + 1;
    newAccount.id = nextID;

    mlabConnection('accounts', query).post('', newAccount, function(errP, resP, bodyP) {
      let response;
      if (errP) {
        response = { "msg" : "Error agregando cuenta." };
        code = 500;
      } else {
        response = newUser;
        code = 200;
      };

      res.status(code).send(response);
    });
  });

}

// Exports
module.exports.getAccounts = getAccounts;
module.exports.postAccount = postAccount;
