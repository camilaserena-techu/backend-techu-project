var request = require('request-json');
var config = require('../config/config.js');
var jwtconfig = require('../config/jwtconfig.js');
const mlabkey = config.mlabkey;
const mlabdbURI = config.mlabdbURI;

/**** USERS ****/

// GET users
function getUsers(req, res) {
  console.log('GET /users');
  dbClient = request.createClient(mlabdbURI);
  query = `q=${JSON.stringify(req.query)}&f={"_id": 0}`;
  dbClient.get(`users?${query}&${mlabkey}`, function (errM, resM, bodyM) {
    let response;
    if (errM) {
      response = { "msg" : "Error consultado usuarios." };
      code = 500;
    } else {
      response = bodyM;
      code = 200;
    };

    res.status(code).send(response);
  });
};

//GET user id
function getUserByID(req, res) {
  console.log('GET /users/id');
  dbClient = request.createClient(mlabdbURI);
  query = `q={"id": ${req.params.id}}&f={"_id": 0}`;
  dbClient.get(`users?${query}&${mlabkey}`, function (errM, resM, bodyM) {
    let response;
    if (errM) {
      response = { "msg" : "Error obteniendo usuario." };
      code = 500;
    } else if (bodyM.length > 0) {
      response = bodyM;
      code = 200;
    } else {
      response = {msg : "Usuario no encontrado."};
      code = 404;
    };

    res.status(code).send(response);
  });
};

//POST user
function postUser(req, res) {
  console.log('POST /users');

  let newUser = {
    'first_name': req.body.first_name,
    'last_name': req.body.last_name,
    'email': req.body.email,
    'password': req.body.password
  };

  dbClient = request.createClient(mlabdbURI);
  query = `f={"_id": 0, "id": 1}&s={"id": -1}&l=1`;
  dbClient.get(`users?${query}&${mlabkey}`, function (errM, resM, bodyM) {
    let response;
    let nextID = bodyM[0].id + 1;
    newUser["id"] = nextID;

    dbClient.post(`users?&${mlabkey}`, newUser, function (errorP, resP, bodyP) {
      if (errorP) {
        response = { "msg" : "Error agregando usuario." };
        code = 500;
      } else {
        response = newUser;
        code = 200;
      };

      res.status(code).send(response);
    });
  });
};

//PUT user id
function putUser(req, res) {
  console.log('PUT /users/id');
  let response;
  dbClient = request.createClient(mlabdbURI);
  query = `q={"id": ${req.params.id}}`;
  let newUser = {
    'id': parseInt(req.params.id),
    'first_name': req.body.first_name,
    'last_name': req.body.last_name,
    'email': req.body.email,
    'password': req.body.password
  };

  dbClient.put(`users?${query}&${mlabkey}`, newUser, function (errP, resP, bodyP) {
    if (errP) {
      response = { "msg" : "Error actualizando usuario." };
      code = 500;
    } else {
      response = bodyP;
      code = 200;
    };
    res.status(code).send(response);
  });
};

//DELETE user id
function deleteUser(req, res){
  console.log('DELETE /users/id');
  dbClient = request.createClient(mlabdbURI);
  query = `q={"id": ${req.params.id}}`;
  dbClient.put(`users?${query}&${mlabkey}`, [], function (errPost, resPost, bodyPost) {
    if (bodyPost.removed == 1) {
      res.status(200).send('Usuario eliminado correctamente.');
    } else {
      console.log(bodyPost);
      res.status(500).send('Error eliminando usuario.');
    }
  });
};


/**** AUTHENTICATION ****/

// LOGIN
function login(req, res) {
  console.log('POST /login');
  let email = req.body.email;
  let pass  = req.body.password;
  if (email && pass) {
    dbClient = request.createClient(mlabdbURI);
    query = `q={email:${JSON.stringify(email)}}`;
    dbClient.get(`users?${query}&${mlabkey}`, function (errGet, resGet, bodyGet) {
      let user = bodyGet[0]
      if (user) {
        if (pass == user.password) {
          let token = jwtconfig.sign({ userID: user.id });
          let session = {"token":token};
          let login = `{"$set":${JSON.stringify(session)}}`;
          dbClient.put(`users?${query}&${mlabkey}`, JSON.parse(login),
            function(errPut, resPut, bodyPut) {
              if (!errPut) {
                res.status(200).send({ "msg" : "Successfull login.", "token" : token });
              } else {
                res.status(500).send({ "msg" : "Error authenticating." });
              };
            });
        } else {
          res.status(404).send({ "msg" : "Wrong credentials." });
        };
      } else {
        res.status(404).send({ "msg" : "Wrong credentials." });
      };
    });
  } else {
    res.status(404).send({ "msg" : "Wrong credentials." });
  };
};

// LOGOUT
function logout(req, res) {
  console.log('POST /logout');
  let email = req.headers.email;
  dbClient = request.createClient(mlabdbURI);
  query = `q={email:${JSON.stringify(email)}}`;
  dbClient.get(`users?${query}&${mlabkey}`, function (errGet, resGet, bodyGet) {
    let user = bodyGet[0];
    if (user) {
      let session = {"token":user.token};
      let login = `{"$unset":${JSON.stringify(session)}}`;
      dbClient.put(`users?${query}&${mlabkey}`, JSON.parse(login),
        function(errPut, resPut, bodyPut) {
          if (!errPut) {
            res.status(200).send({ "msg" : "Successfull logout." });
          } else {
            res.status(500).send({ "msg" : "Error." });
          };
        });
    } else {
      res.status(500).send({ "msg" : "Error." });
    };
  });
};

// LOGIN w/token
function loginToken(req, res) {
  console.log('POST /login');
  let email = req.body.email;
  let pass  = req.body.password;
  if (email && pass) {
    dbClient = request.createClient(mlabdbURI);
    query = `q={email:${JSON.stringify(email)}}`;
    dbClient.get(`users?${query}&${mlabkey}`, function (errGet, resGet, bodyGet) {
      let user = bodyGet[0]
      if (user) {
        if (pass == user.password) {
          let token = jwtconfig.sign({ userID: user.id });
          res.status(200).send({ "msg" : "Successfull login.", "data": {"token" : token, "user_id" : user.id } });
        } else {
          res.status(404).send({ "msg" : "Wrong credentials." });
        };
      } else {
        res.status(404).send({ "msg" : "Wrong credentials." });
      };
    });
  } else {
    res.status(404).send({ "msg" : "Wrong credentials." });
  };
};


//LOGOUT w/token
function logoutToken(req, res) {
  console.log('POST /logout (token)');
  token = req.headers.authorization.split(' ')[1];
  jti = jwtconfig.decode(token).jti;
  revoked = {'tokenid' : jti};
  dbClient = request.createClient(mlabdbURI);
  dbClient.post(`blacklist?${mlabkey}`, revoked, function(errPost, resPost, bodyPost) {
    if (!errPost) {
      res.status(200).send({ "msg" : "Successfull logout." });
    } else {
      res.status(500).send({ "msg" : "Error." });
    }
  });
}


// Exports
module.exports.getUsers = getUsers;
module.exports.getUser = getUserByID;
module.exports.postUser = postUser;
module.exports.putUser = putUser;
module.exports.deleteUser = deleteUser;
module.exports.login = loginToken;
module.exports.logout = logoutToken;
