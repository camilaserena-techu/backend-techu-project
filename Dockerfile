FROM node

WORKDIR /api-uruguay

ADD . /api-uruguay

RUN npm install

EXPOSE 3000

CMD ["npm", "start"]
