var express = require('express');
var cors = require('cors')
var bodyParser = require('body-parser');
var config = require('./config/config.js');
var jwtconfig = require('./config/jwtconfig.js');
var users = require('./app/users.js');
var accounts = require('./app/accounts.js');
var auth = require('./app/authentication.js');

var port = config.port;
var URI = config.uri;

var app = express();
app.use(cors());
app.use(bodyParser.json());
app.use(jwtconfig.config.unless({path: [`${URI}/login`, `${URI}/signin`]}));

/**** AUTHENTICATION ****/
app.post(URI + '/login', auth.login);
app.post(URI + '/login', auth.login);
app.post(URI + '/signin', auth.signin);

/**** USERS ****/
app.get(URI + '/users', users.getUsers);
app.get(URI + '/users/:id', users.getUser);
app.post(URI + '/users', users.postUser);
app.put(URI + '/users/:id', users.putUser);
app.delete(URI + '/users/:id', users.deleteUser);

/**** ACCOUNTS ****/
app.get(URI + '/accounts', accounts.getAccounts);
app.get(URI + '/accounts/:id', accounts.getAccount);
app.post(URI + '/accounts', accounts.postAccount);
app.put(URI + '/accounts:id', accounts.putAccount);
app.delete(URI + '/accounts/:id', accounts.deleteAccount);

/**** TRANSACTIONS ****/
app.get(URI + '/accounts/:id/transactions', accounts.getTransactionsAccount);

app.listen(port, function () {
  console.log('CORS-enabled web server listening on port ' + port);
});
