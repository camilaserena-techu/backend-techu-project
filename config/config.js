var request = require('request-json');
const uri = '/api-uruguay/v1'
const port = process.env.PORT || 3000;

const mlabkey = 'apiKey=Z1xSoB-Pd61t-VQkVc0cvzDkArA-5Wi9';
const mlabdbURI = 'https://api.mlab.com/api/1/databases/techubduruguay/collections/'

function getconnection(resource, query) {
  dbURL = `${mlabdbURI}${resource}?${mlabkey}`
  if (query) {
    dbURL = dbURL + `&${query}`;
  }
  return request.createClient(dbURL);
}

// Exports
module.exports.uri = uri;
module.exports.port = port;
module.exports.mlabkey = mlabkey;
module.exports.mlabdbURI = mlabdbURI;
module.exports.connection = getconnection;
