var jwt = require('express-jwt');
var jwtt = require('jsonwebtoken');
var request = require('request-json');
var uniqid = require('uniqid');
var config = require('../config/config.js');
var jwtsecret = 'sdhvaowfhcnadicaowejrihwepr';
const mlabkey = config.mlabkey;
const mlabdbURI = config.mlabdbURI;

var isRevokedCallback = function(req, payload, done){
  jti = payload.jti;
  dbClient = request.createClient(mlabdbURI);
  query = `q=${JSON.stringify({'tokenid' : jti})}`;
  dbClient.get(`blacklist?${query}&${mlabkey}`, function (errM, resM, bodyM) {
    if (errM) {
      done(errM);
    } else {
      revoked = bodyM.length > 0;
      done(null,revoked);
    };
  });
};

var config = jwt({ secret : jwtsecret, isRevoked : isRevokedCallback });

var sign = function(payload) {
  let jwtid = uniqid();
  return jwtt.sign(payload, jwtsecret, { 'jwtid': jwtid, 'expiresIn': '1h' });
}

var decode = function(token) {
  return jwtt.decode(token);
}

var blacklist = function(token) {
  return true;
}

// Exports
module.exports.config = config;
module.exports.sign = sign;
module.exports.decode = decode;
module.exports.blacklist = blacklist;
